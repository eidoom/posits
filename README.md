# posits

[C reference](https://en.cppreference.com/w/c)

Uses [x86 GCC `_Float16`](https://gcc.gnu.org/onlinedocs/gcc/Half-Precision.html) and [`_Float128`](https://gcc.gnu.org/onlinedocs/gcc/Floating-Types.html#Floating-Types).

[SoftPosit](https://gitlab.com/cerlane/SoftPosit)
```shell
wget https://gitlab.com/cerlane/SoftPosit/-/archive/0.4.1/SoftPosit-0.4.1.tar.gz
tar xf SoftPosit-0.4.1.tar.gz
cd SoftPosit-0.4.1
LIB=$PWD
cd build/Linux-x86_64-GCC
sed -i 's/#-march=core-avx2/-march=native -mtune=native/' Makefile
make -j`nproc`
```
Here
```shell
make
./main
```

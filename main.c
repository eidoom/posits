#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "softposit.h"

void examples(void)
{
	{
		// A 8-bit example on how to use the code to add:
		posit8_t pA, pB, pZ;
		pA = castP8(0xF2);
		pB = castP8(0x23);

		pZ = p8_add(pA, pB);

		// To check answer by converting it to double
		double dZ = convertP8ToDouble(pZ);
		printf("dZ: %.15f\n", dZ);

		// To print result in binary
		uint8_t uiZ = castUI(pZ);
		printBinary((uint64_t*)&uiZ, 8);
	}

	{
		// A 16-bit example on how to use the code to multiply:
		posit16_t pA, pB, pZ;
		pA = castP16(0x0FF2);
		pB = castP16(0x2123);

		pZ = p16_mul(pA, pB);

		// To check answer by converting it to double
		double dZ = convertP16ToDouble(pZ);
		printf("dZ: %.15f\n", dZ);

		// To print result in binary
		uint16_t uiZ = castUI(pZ);
		printBinary((uint64_t*)&uiZ, 16);
	}

	{
		// A 24-bit (es=2) example on how to use the code:
		posit_2_t pA, pB, pZ;
		pA.v = 0xF2;				// this is to set the bits (method 1)
		pB = castPX2(0x23); // this is to set the bits (method 2)

		pZ = pX2_add(pA, pB, 24);

		// To check answer by converting it to double
		double dZ = convertPX2ToDouble(pZ);
		printf("dZ: %.40f\n", dZ);

		// To print result in binary
		printBinaryPX((uint32_t*)&pZ.v, 24);

		// To print result as double
		printf("result: %.40f\n", convertPX2ToDouble(pZ));
	}

	{
		// For deep learning, please use quire.
		// Convert double to posit
		posit16_t pA = convertDoubleToP16(1.02783203125);
		posit16_t pB = convertDoubleToP16(0.987060546875);
		posit16_t pC = convertDoubleToP16(0.4998779296875);
		posit16_t pD = convertDoubleToP16(0.8797607421875);

		quire16_t qZ;

		// Set quire to 0
		qZ = q16_clr(qZ);

		// accumulate products without roundings
		qZ = q16_fdp_add(qZ, pA, pB);
		qZ = q16_fdp_add(qZ, pC, pD);

		// Convert back to posit
		posit16_t pZ = q16_to_p16(qZ);

		// To check answer
		double dZ = convertP16ToDouble(pZ);
	}
}

int main(int argc, char* argv[])
{
	int s = 0,
			n = 100,
			e = n / 10;
	double d = 0.1;

	{
		_Float16 a = (_Float16)s,
						 b = (_Float16)d;

		for (int i; i < n; ++i) {
			a += b;
		}

		printf("%4s %.1e\n", "f16", fabs((_Float16)e - a));
	}

	{
		float a = (float)s,
					b = (float)d;

		for (int i; i < n; ++i) {
			a += b;
		}

		printf("%4s %.1e\n", "f32", fabs((float)e - a));
	}

	{
		double a = (double)s,
					 b = d;

		for (int i; i < n; ++i) {
			a += b;
		}

		printf("%4s %.1e\n", "f64", fabs((double)e - a));
	}

	{
		_Float128 a = (_Float128)s,
						 b = (_Float128)d;

		for (int i; i < n; ++i) {
			a += b;
		}

		printf("%4s %.1e\n", "f128", fabs((_Float128)e - a));
	}

	{
		posit8_t a = i32_to_p8(s),
						 b = convertDoubleToP8(d);

		for (int i; i < n; ++i) {
			a = p8_add(a, b);
		}

		a = p8_sub(i32_to_p8(e), a);

		printf("%4s %.1e\n", "p8", fabs(convertP8ToDouble(a)));
	}

	{
		posit16_t a = i32_to_p16(s),
							b = convertDoubleToP16(d);

		for (int i; i < n; ++i) {
			a = p16_add(a, b);
		}

		a = p16_sub(i32_to_p16(e), a);

		printf("%4s %.1e\n", "p16", fabs(convertP16ToDouble(a)));
	}

	{
		posit32_t a = i32_to_p32(s),
							b = convertDoubleToP32(d);

		for (int i; i < n; ++i) {
			a = p32_add(a, b);
		}

		a = p32_sub(i32_to_p32(e), a);

		printf("%4s %.1e\n", "p32", fabs(convertP32ToDouble(a)));
	}

	return EXIT_SUCCESS;
}

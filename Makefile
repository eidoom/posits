LIB=$(HOME)/tar/SoftPosit-0.4.1

main: main.c
	gcc -lm -o $@ $< $(LIB)/build/Linux-x86_64-GCC/softposit.a -I$(LIB)/source/include -O2 -march=native -mtune=native
